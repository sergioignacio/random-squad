var totalKills = 0;
var kdPromedio = 0;
var partidas = 0; 
var victorias = 0;

var options = {
      useEasing: true, 
      useGrouping: true, 
      separator: '.', 
      decimal: ',', 
    };




$.when(requests[0], requests[1], requests[2],requests[3],requests[4],requests[5],requests[6],requests[7],requests[8]).then(function() {
    var totalPartidas = 0;
    var totalKd = 0; 

    console.log(stats.length);
    for(x in stats){
        totalKills = totalKills +stats[x].stats["kills_solo"];

        totalPartidas = totalPartidas +stats[x].stats["matchesplayed_squad"]; 
        totalKd = totalKd +stats[x].stats["kd_squad"];
        victorias = victorias +stats[x].stats["placetop1_squad"];   
    }  
    victorias = parseFloat(victorias / (stats.length/2));
    partidas = totalPartidas / stats.length;
    kdPromedio = parseFloat(totalKd / stats.length).toFixed(1);


    var vc = new CountUp('vId', 0, victorias, 0, 2.5, options);
    var pt = new CountUp('pjId', 0, partidas, 0, 2.5, options);     
    var kda = new CountUp('kdaId', 0, kdPromedio, 1, 2.5, options);        
    var kills = new CountUp('kId', 0, totalKills, 0, 2.5, options); 
   
        
    os.on('enter', '.onScreen', (element, event) => {
        if (!vc.error) {
            vc.start();
        } else {
          console.error(vc.error);
        }
        if (!kills.error) {
            kills.start();
         } else {
           console.error(kills.error);
         }
         if (!kda.error) {
            kda.start();
        } else {
          console.error(kda.error);
        }    
        if (!pt.error) {
            pt.start();
         } else {
            console.error(pt.error);
        }
        os.destroy();
    });    
});