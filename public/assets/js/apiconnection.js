var endpoint = "https://fortnite-public-api.theapinetwork.com/prod09/users/public/br_stats";

var stats = [];

var fetchData = function(query,dataURL) {
    return $.ajax({
		type: "Get",
		dataType: 'json',
		data: query,
		jsonp: false,
		url: endpoint, 
        contentType: 'application/json',
        success : function(data){
            //console.log(data);
            stats.push(data);
        },
		error : function(httpReq,status,exception){
			console.log(status+" "+exception);
		}
    });
}

var requests =  [];



var members ='{ "members" : [' +
'{ "user":"Sierz" , "uid":"ffa260a1ddbc49ccba550fb527561f98", "platform":"pc" },' +
'{ "user":"NotGooberT" , "uid":"2453cb58e7684994add8a6e2c73a89d8", "platform":"pc" },' +
'{ "user":"Alucard" , "uid":"d4552f3458df4146911cfc59858d3f37", "platform":"pc" },' +
'{ "user":"Yanyu" , "uid":"d71c5814ca4c4373a9dda54409a20817", "platform":"pc" },' +
'{ "user":"Connie" , "uid":"4f5e44bcf58d426e9c9f0a5478d64d89", "platform":"pc" },' +
'{ "user":"SoulBlack" , "uid":"28d2e445160c4022b56f51a0daaf5519", "platform":"pc" },' +
'{ "user":"LordTanK" , "uid":"da3a23c51abf4845a46d59a5acfb5552", "platform":"pc" },' +
'{ "user":"Juantástico" , "uid":"4d791ca645e64667aef8d282d804bd0a", "platform":"pc" },' +
'{ "user":"Duty" , "uid":"acb63607c1c741519dab5068a6f44e67", "platform":"pc" } ]}';

var obj = JSON.parse(members);

var members = obj.members;

for (x in members){
	requests.push( 
		fetchData({
			"user_id": members[x].uid,
			"platform": members[x].platform
		},members[x].url) )
}